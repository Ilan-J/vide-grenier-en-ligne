FROM php:8.3-apache

# Port http & ssl
EXPOSE 80
EXPOSE 443

RUN apt-get update
RUN apt-get upgrade -y

# Get Composer
COPY --from=composer/composer:latest-bin /composer /usr/bin/composer
RUN composer self-update

# PHP extensions
RUN docker-php-ext-install \
    pdo \
    pdo_mysql \
    && docker-php-ext-enable \
    pdo_mysql

# Apache2 modules
RUN a2enmod rewrite
# Apache2 conf
COPY 000-default.conf          /etc/apache2/sites-available

# Install persistent dependencies
RUN apt-get install -y \
    zip \
    unzip \
    libzip-dev

# App dir
RUN mkdir /var/www/vide-grenier
WORKDIR /var/www/vide-grenier
# App
COPY ../App                        ./App
COPY ../Core                       ./Core
COPY ../public                     ./public
COPY ../composer.json              .
# COPY LICENSE                    .
# COPY README.md                  .

RUN chown -R www-data .

RUN composer install
