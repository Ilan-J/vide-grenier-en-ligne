<?php

namespace App\Utility;

class Cookie {
    
    /**
     * Get:
     * @access public
     * @param string $key
     * @return mixed
     */
    public static function get(string $key) {
        return $_COOKIE[$key];
    }

    /**
     * Put:
     * @access public
     * @param string $key
     * @param mixed $value
     * @param integer $expiry
     * @return boolean
     */
    public static function put(string $key, $value, int $expiry) : bool {
        return setcookie($key, $value, time() + $expiry, "/");
    }

    /**
     * Delete:
     * @access public
     * @param string $key
     * @return void
     */
    public static function delete(string $key) {
        self::put($key, "", time() - 42000);
    }

    /**
     * Exists:
     * @access public
     * @param string $key
     * @return boolean
     */
    public static function exists(string $key) : bool {
        return isset($_COOKIE[$key]);
    }
}
