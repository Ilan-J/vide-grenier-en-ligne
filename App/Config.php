<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'db';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'videgrenierenligne';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'webapplication';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = '653rag9T';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;

    /**
     * Remember me cookie name
     * @var string
     */
    const REMEMBER_ME = "RememberMeyouMust";

    /**
     * Cookie default expiry in seconds
     * 1m = 60s
     * 1h = 3600s
     * 1d = 86400s
     * @var integer
     */
    const COOKIE_DEFAULT_EXPIRY = 86400 * 30;   // 30 days

}
