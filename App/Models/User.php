<?php

namespace App\Models;

use App\Utility\Hash;
use Core\Model;
use App\Core;
use Exception;
use App\Utility;

/**
 * User Model:
 */
class User extends Model {

    /**
     * Crée un utilisateur
     */
    public static function createUser($data) {
        $db = static::getDB();

        $stmt = $db->prepare('INSERT INTO users(username, email, password, salt) VALUES (:username, :email, :password,:salt)');

        $stmt->bindParam(':username', $data['username']);
        $stmt->bindParam(':email', $data['email']);
        $stmt->bindParam(':password', $data['password']);
        $stmt->bindParam(':salt', $data['salt']);

        $stmt->execute();

        return $db->lastInsertId();
    }

    public static function getById($userID)
    {
        $db = static::getDB();

        $stmt = $db->prepare("SELECT * FROM users WHERE ( users.id = :user_id) LIMIT 1");

        $stmt->bindParam(':user_id', $userID);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public static function getByLogin($login)
    {
        $db = static::getDB();

        $stmt = $db->prepare("
            SELECT * FROM users WHERE ( users.email = :email) LIMIT 1
        ");

        $stmt->bindParam(':email', $login);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public static function getIdByCookie($cookie)
    {
        $db = static::getDB();

        $sql = 'SELECT cookies.user_id FROM cookies WHERE (cookies.cookie = :cookie) LIMIT 1';
        $stmt = $db->prepare($sql);

        $stmt->bindParam(':cookie', $cookie);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public static function getCookies($userID) {
        $db = static::getDB();

        $sql = 'SELECT cookies.cookie FROM cookies WHERE (cookies.user_id = :user_id)';
        $stmt = $db->prepare($sql);

        $stmt->bindParam(':user_id', $userID);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public static function setCookie($userID, $cookie) {
        $db = static::getDB();

        $sql = 'INSERT cookies (cookie, user_id) VALUE (:cookie, :user_id)';
        $stmt = $db->prepare($sql);

        $stmt->bindParam(':cookie', $cookie);
        $stmt->bindParam(':user_id', $userID);
        $stmt->execute();

        return true;
    }

    /**
     * ?
     * @access public
     * @return string|boolean
     * @throws Exception
     */
    public static function login() {
        $db = static::getDB();

        $stmt = $db->prepare('SELECT * FROM articles WHERE articles.id = ? LIMIT 1');

        $stmt->execute([$id]);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


}
