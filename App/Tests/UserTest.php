<?php
namespace App\Tests;
use App\Controllers\User;
class UserTest extends \PHPUnit\Framework\TestCase
{
    public function testRegister()
    {
        $_POST = ['submit' => true];
        $controller = new User('/register');

        ob_start();
        $controller->registerAction();
        $output = ob_get_clean();

        $this->assertStringContainsString('Le nom d&#039;utilisateur est requis', $output);
        $this->assertStringContainsString('L&#039;adresse email est requise', $output);
        $this->assertStringContainsString('Le mot de passe est requis', $output);
        $this->assertStringContainsString('Les deux mots de passe donnés ne sont pas identiques', $output);
    }
    
    public function testRegisterWithInvalidData()
    {
        $_POST = ['submit' => true, 'username' => 'test', 'email' => 'blabla', 'password' => 'tesst'];
        $controller = new User('/register');

        ob_start();
        $controller->registerAction();
        $output = ob_get_clean();

        $this->assertStringNotContainsString('Le nom d&#039;utilisateur est requis', $output);
        $this->assertStringContainsString('L&#039;adresse email n&#039;est pas valide', $output);
        $this->assertStringContainsString('Le mot de passe doit contenir un caractère spécial', $output);
        $this->assertStringContainsString('Les deux mots de passe donnés ne sont pas identiques', $output);
    }
}