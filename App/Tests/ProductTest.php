<?php
namespace App\Tests;
use App\Controllers\Product;
class ProductTest extends \PHPUnit\Framework\TestCase
{
    public function testAddArticle()
    {
        $_POST = ['submit' => true];
        $controller = new Product('/product');

        ob_start();
        $controller->indexAction();
        $output = ob_get_clean();

        $this->assertStringContainsString('Le titre est requis', $output);
        $this->assertStringContainsString('La description est requise', $output);
        $this->assertStringContainsString('La ville est requise', $output);
        $this->assertStringContainsString('L&#039;image est requise', $output);
    }

    public function testAddArticleWithoutInvalidImage()
    {
        $_POST = ['submit' => true, 'name' => 'test', 'description' => 'blabla', 'city' => 'tesst'];
        $controller = new Product('/product');

        ob_start();
        $controller->indexAction();
        $output = ob_get_clean();

        $this->assertStringNotContainsString('Le titre est requis', $output);
        $this->assertStringNotContainsString('La description est requise', $output);
        $this->assertStringNotContainsString('La ville est requise', $output);
        $this->assertStringContainsString('L&#039;image est requise', $output);
    }
}