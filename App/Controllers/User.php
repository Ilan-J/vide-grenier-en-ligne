<?php

namespace App\Controllers;

use App\Config;
use App\Model\UserRegister;
use App\Models\Articles;
use App\Utility\Cookie;
use App\Utility\Hash;
use App\Utility\Session;
use \Core\View;
use Exception;
use http\Env\Request;
use http\Exception\InvalidArgumentException;

/**
 * User controller
 */
class User extends \Core\Controller
{

    /**
     * Affiche la page de login
     */
    public function loginAction()
    {
        if(isset($_POST['submit'])) {
            $f = $_POST;

            // TODO: Validation

            $this->login($f);
        } elseif (Cookie::exists(Config::REMEMBER_ME)) {

            $this->loginWithCookie();
        }

        if (isset($_SESSION['user']['id'])) {
            // Si login OK, redirige vers le compte
            header('Location: /account');
        }

        View::renderTemplate('User/login.html');
    }

    /**
     * Page de création de compte
     */
    public function registerAction()
    {
        if(isset($_POST['submit'])){
            $errors = [];
            $f = $_POST;

            // Validation du nom d'utilisateur
            if (empty($f['username'])) {
                $errors[] = 'Le nom d\'utilisateur est requis';
            } else {
                if (mb_strlen($f['username']) > 100) {
                    $errors[] = 'Le nom d\'utilisateur ne doit pas dépasser les 100 caractères';
                }
            }

            // Validation de l'email
            if (empty($f['email'])) {
                $errors[] = 'L\'adresse email est requise';
            } else {
                if (mb_strlen($f['email']) > 254) {
                    $errors[] = 'L\'adresse email ne doit pas dépasser 254 caractères';
                }
                if (!filter_var($f['email'], FILTER_VALIDATE_EMAIL)) {
                    $errors[] = 'L\'adresse email n\'est pas valide';
                }
            }

            // Validation du mot de passe
            if (empty($f['password'])) {
                $errors[] = 'Le mot de passe est requis';
            } else {
                if (mb_strlen($f['password']) > 254) {
                    $errors[] = 'Le mot de passe ne doit pas dépasses 254 caractères';
                }
                $specialCharsPattern = "/[!@#$%^&*(),.?\":{}|<>]/";
                if (!preg_match($specialCharsPattern, $f['password'])) {
                    $errors[] = 'Le mot de passe doit contenir un caractère spécial';
                }
            }

            // Validation de la vérfication du mot de passe
            if (empty($f['password-check']) || $f['password-check'] != $f['password']) {
                $errors[] = 'Les deux mots de passe donnés ne sont pas identiques';
            }

            if (empty($errors)) {
                $this->register($f);
                $this->loginAction($f);
            }
            
            // Si des erreurs, les afficher
            if (!empty($errors)) {
                View::renderTemplate('User/register.html', [
                    'errors' => $errors,
                    'form_data' => $f
                ]);
            }
        } else {
            View::renderTemplate('User/register.html');
        }
    }

    /**
     * Affiche la page du compte
     */
    public function accountAction()
    {
        $articles = Articles::getByUser($_SESSION['user']['id']);

        View::renderTemplate('User/account.html', [
            'articles' => $articles
        ]);
    }

    /*
     * Fonction privée pour enregister un utilisateur
     */
    private function register($data)
    {
        try {
            // Generate a salt, which will be applied to the during the password
            // hashing process.
            $salt = Hash::generateSalt(32);

            $userID = \App\Models\User::createUser([
                "email" => $data['email'],
                "username" => $data['username'],
                "password" => Hash::generate($data['password'], $salt),
                "salt" => $salt
            ]);

            return $userID;

        } catch (Exception $ex) {
            // TODO : Set flash if error : utiliser la fonction en dessous
            /* Utility\Flash::danger($ex->getMessage());*/
        }
    }

    private static function createRememberCookie($userID) {
        $hash = Hash::generateUnique();
        if (!\App\Models\User::setCookie($userID, $hash)) {
            return false;
        }
        
        $cookie = Config::REMEMBER_ME;
        $expiry = Config::COOKIE_DEFAULT_EXPIRY;
        return(Cookie::put($cookie, $hash, $expiry));
    }

    private function login($data){
        try {
            if(!isset($data['email'])){
                throw new Exception('TODO');
            }

            $user = \App\Models\User::getByLogin($data['email']);

            if (Hash::generate($data['password'], $user['salt']) !== $user['password']) {
                return false;
            }

            // Create a remember me cookie if the user has selected
            // the option to remained logged in on the login form.
            // https://github.com/andrewdyer/php-mvc-register-login/blob/development/www/app/Model/UserLogin.php#L86
            if (isset($data['remember_me'])) {
                self::createRememberCookie($user['id']);
            }

            $_SESSION['user'] = array(
                'id' => $user['id'],
                'username' => $user['username'],
            );

            return true;

        } catch (Exception $ex) {
            // TODO : Set flash if error
            /* Utility\Flash::danger($ex->getMessage());*/
        }
    }

    /**
     * Login with cookie. Returns true if user logged in
     * otherwise returns false.
     * @access private
     * @return boolean
     */
    private function loginWithCookie() : bool {
        // Retrieve cookie (User)
        $cookie = Cookie::get(Config::REMEMBER_ME);
        // Find user associated to cookie
        $userID = \App\Models\User::getIdByCookie($cookie);

        if (!$userID) {
            return false;
        }
        $user = \App\Models\User::getById($userID);
        
        $_SESSION['user'] = array(
            'id' => $user['id'],
            'username' => $user['username'],
        );

        return true;
    }

    /**
     * Logout: Delete cookie and session. Returns true if everything is okay,
     * otherwise turns false.
     * @access public
     * @return boolean
     * @since 1.0.2
     */
    public function logoutAction() {

        if (Cookie::exists(Config::REMEMBER_ME)) {
            // Delete the users remember me cookie if one has been stored.
            // https://github.com/andrewdyer/php-mvc-register-login/blob/development/www/app/Model/UserLogin.php#L148
            Cookie::delete(Config::REMEMBER_ME);
        }
        // Destroy all data registered to the session.

        $_SESSION = array();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();

        header ("Location: /");

        return true;
    }

}
