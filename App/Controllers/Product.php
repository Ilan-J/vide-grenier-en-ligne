<?php

namespace App\Controllers;

use App\Models\Articles;
use App\Utility\Upload;
use \Core\View;

/**
 * Product controller
 */
class Product extends \Core\Controller
{
    /**
     * Affiche la page d'ajout
     * @return void
     */
    public function indexAction()
    {
        if (isset($_POST['submit'])) {
            $errors = [];
            $f = $_POST;

            // Validation du titre
            if (empty($f['name'])) {
                $errors[] = 'Le titre est requis';
            } else {
                if (mb_strlen($f['name']) > 200) {
                    $errors[] = 'Le titre dépasse 200 caractères';
                }
            }

            // Validation de la description
            if (empty($f['description'])) {
                $errors[] = 'La description est requise';
            } else {
                if (mb_strlen($f['description']) > 1000) {
                    $errors[] = 'La descritpion dépasse 1000 caractères';
                }
            }

            // Validation de la ville
            if (empty($f['city'])) {
                $errors[] = 'La ville est requise';
            }

            // Validation de l'image
            if (empty($_FILES['picture']) && empty($_FILES['picture']['name'])) {
                $errors[] = "L'image est requise";
            } else {
                // Vérifier les erreurs de téléchargement
                if ($_FILES['picture']['error'] !== UPLOAD_ERR_OK) {
                    switch ($_FILES['picture']['error']) {
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            $errors[] = 'La taille de l\'image ne doit pas dépasser 2MB.';
                            break;
                        default:
                            $errors[] = 'Erreur lors du téléchargement de l\'image.';
                    }
                } else {
                    // Validation du type de fichier
                    $allowedTypes = ['image/jpeg', 'image/png', 'image/jpg'];
                    if (!in_array($_FILES['picture']['type'], $allowedTypes)) {
                        $errors[] = 'Le format de l\'image est invalide. Seuls les formats JPEG, PNG et JPG sont acceptés.';
                    }
            
                    // Validation de la taille de l'image
                    if ($_FILES['picture']['size'] > 2 * 1024 * 1024) {
                        $errors[] = 'La taille de l\'image ne doit pas dépasser 2MB.';
                    }
                }
            }

            if (empty($errors)) {
                try {
                    $f['user_id'] = $_SESSION['user']['id'];
                    $id = Articles::save($f);

                    $pictureName = Upload::uploadFile($_FILES['picture'], $id);

                    Articles::attachPicture($id, $pictureName);

                    header('Location: /product/' . $id);
                    exit;
                } catch (\Exception $e) {
                    $errors[] = 'Une erreur est survenue lors de l\'enregistrement de l\'article.';
                }
            }

            // Si des erreurs, les afficher
            if (!empty($errors)) {
                View::renderTemplate('Product/Add.html', [
                    'errors' => $errors,
                    'form_data' => $f
                ]);
            }
        } else {
            View::renderTemplate('Product/Add.html');
        }
    }

    public function test() {
        return 1;
    }
    /**
     * Affiche la page d'un produit
     * @return void
     */
    public function showAction()
    {
        $id = $this->route_params['id'];

        try {
            Articles::addOneView($id);
            $suggestions = Articles::getSuggest();
            $article = Articles::getOne($id);
        } catch (\Exception $e){
            var_dump($e);
        }

        View::renderTemplate('Product/Show.html', [
            'article' => $article[0],
            'suggestions' => $suggestions
        ]);
    }
}
